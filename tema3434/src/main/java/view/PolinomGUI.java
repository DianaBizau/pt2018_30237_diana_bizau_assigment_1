package view;
import javax.swing.*;

import controller.Polinom;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import model.Monom;

public class PolinomGUI 
{

	public static void main(String[] args) 
	{
		JFrame frame=new JFrame("Calculator Polinom");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500,400);
		JPanel panel = new JPanel();
		panel.setLayout(null);
		
		JLabel l1 = new JLabel("Polinom1: ");
		l1.setForeground(Color.blue);
		l1.setFont(new Font("CAMBRIA", Font.BOLD, 18));
		final JTextArea f1 = new JTextArea();
		panel.add(l1);
		l1.setBounds(5,10,100,25);
		panel.add(f1);
		f1.setBounds(120, 10, 300, 20);
		
		
		JLabel l2 = new JLabel("Polinom2: ");
		l2.setForeground(Color.blue);
		l2.setFont(new Font("CAMBRIA", Font.BOLD, 18));
		final JTextArea f2 = new JTextArea();
		panel.add(l2);
		l2.setBounds(5,50,100,25);
		panel.add(f2);
		f2.setBounds(120, 50, 300, 20);
		
		
		JButton b1 = new JButton("+");
		b1.setForeground(Color.blue);
		b1.setFont(new Font("CAMBRIA", Font.BOLD, 22));
		panel.add(b1);
		b1.setBounds(80,110,50,35);
		
		JButton b2 = new JButton("-");
		b2.setForeground(Color.blue);
		b2.setFont(new Font("CAMBRIA", Font.BOLD, 22));
		panel.add(b2);
		b2.setBounds(180,110,50,35);
		
		JButton b3 = new JButton("*");
		b3.setForeground(Color.blue);
		b3.setFont(new Font("CAMBRIA", Font.BOLD, 22));
		panel.add(b3);
		b3.setBounds(280,110,50,35);
		
		JButton b4 = new JButton("/");
		b4.setForeground(Color.blue);
		b4.setFont(new Font("CAMBRIA", Font.BOLD, 22));
		panel.add(b4);
		b4.setBounds(380,110,50,35);
		
		JButton b5 = new JButton("Derivare");
		b5.setForeground(Color.blue);
		b5.setFont(new Font("CAMBRIA", Font.BOLD, 14));
		panel.add(b5);
		b5.setBounds(110,180,100,35);
		
		JButton b6 = new JButton("Integrare");
		b6.setForeground(Color.blue);
		b6.setFont(new Font("CAMBRIA", Font.BOLD, 14));
		panel.add(b6);
		b6.setBounds(310,180,100,35);
		
		
		JLabel l3 = new JLabel("Rezultat: ");
		l3.setForeground(Color.red);
		l3.setFont(new Font("CAMBRIA",Font.BOLD, 18));
		final JTextArea f3 = new JTextArea();
		panel.add(l3);
		l3.setBounds(5,260,100,25);
		panel.add(f3);
		f3.setBounds(120,260,300,20);
		
		b1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String x=f1.getText();
				String y=f2.getText();
				Polinom p1=new Polinom(x);
				Polinom p2=new Polinom(y);
				ArrayList <Monom> pol2=new ArrayList<Monom>(10000);
				pol2=p1.adunare(p2);
				int c,ex,s=0,i;
				String C,E,Z;
				f3.setFocusable(true);
				f3.setText(null);
				for(i=0;i<pol2.size();i++)
				{
					c=pol2.get(i).getCoeficient();
					ex=pol2.get(i).getExponent();
					if(c!=0)
					{
						C=Integer.toString(c);
						E=Integer.toString(ex);
						Z=C+"x^"+E;
						f3.append(Z);
					}
					if((i+1)!=pol2.size())
						s=pol2.get(i+1).getCoeficient();
					if(s>0)
						f3.append("+");
					s=0;
					
				}
			}
		});
		b2.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String x=f1.getText();
				String y=f2.getText();
				Polinom p1=new Polinom(x);
				Polinom p2=new Polinom(y);
				ArrayList <Monom> pol2=new ArrayList<Monom>(1000);
				pol2=p1.scadere(p2);
				int c,ex,s=0,i;
				String C,E,Z;
				f3.setFocusable(true);
				f3.setText(null);
				for(i=0;i<pol2.size();i++)
				{
					c=pol2.get(i).getCoeficient();
					ex=pol2.get(i).getExponent();
					if(c!=0)
					{
						C=Integer.toString(c);
						E=Integer.toString(ex);
						Z=C+"x^"+E;
						f3.append(Z);
					}
					if((i+1)!=pol2.size())
						s=pol2.get(i+1).getCoeficient();
					if(s>0)
						f3.append("+");
					s=0;
					
				}
			}
		});
		b3.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String x=f1.getText();
				String y=f2.getText();
				Polinom p1=new Polinom(x);
				Polinom p2=new Polinom(y);
				ArrayList <Monom> pol2=new ArrayList<Monom>(1000);
				pol2=p1.inmultire(p2);
				int c,ex,s=0,i;
				String C,E,Z;
				f3.setFocusable(true);
				f3.setText(null);
				for(i=0;i<pol2.size();i++)
				{
					c=pol2.get(i).getCoeficient();
					ex=pol2.get(i).getExponent();
					if(c!=0)
					{
						C=Integer.toString(c);
						E=Integer.toString(ex);
						Z=C+"x^"+E;
						f3.append(Z);
					}
					if((i+1)!=pol2.size())
						s=pol2.get(i+1).getCoeficient();
					if(s>0)
						f3.append("+");
					s=0;
					
				}
			}
		});
		b5.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String x=f1.getText();
				Polinom pol1=new Polinom(x);
				ArrayList<Monom> pol2 =new ArrayList<Monom>(10000);
				pol2=pol1.derivare();
				int c,ex,s=0,i;
				String C,E,Z;
				f3.setEditable(true);
				f3.setText(null);
				for(i=0;i<pol2.size();i++)
				{
					c=pol2.get(i).getCoeficient();
					ex=pol2.get(i).getExponent();
					if(c!=0)
					{
						C=Integer.toString(c);
						E=Integer.toString(ex);
						Z=C+"x^"+E;
						f3.append(Z);
					}
					if((i+1)!=pol2.size())
						s=pol2.get(i+1).getCoeficient();
					if(s>0)
						f3.append("+");
					s=0;
				}
			}
		});
		b6.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String x=f1.getText();
				Polinom pol1=new Polinom(x);
				ArrayList<Monom> pol2 =new ArrayList<Monom>(10000);
				pol2=pol1.integrare();
				int c,ex,s=0,i;
				String C,E,Z;
				f3.setEditable(true);
				f3.setText(null);
				for(i=0;i<pol2.size();i++)
				{
					c=pol2.get(i).getCoeficient();
					ex=pol2.get(i).getExponent();
					if(c!=0)
					{
						C=Integer.toString(c);
						E=Integer.toString(ex);
						Z=C+"x^"+E;
						f3.append(Z);
					}
					if((i+1)!=pol2.size())
						s=pol2.get(i+1).getCoeficient();
					if(s>0)
						f3.append("+");
					s=0;
				}
			}
		});
		
		frame.setContentPane(panel);
		frame.setVisible(true);
	}
}
