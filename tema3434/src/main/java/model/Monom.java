package model;

public class Monom 
{
	private int exponent;
	private int coeficient;


	public Monom(int exp, int coef)
	{
		exponent=exp;
		coeficient=coef;
	}
	public int getExponent() 
	{
		return exponent;
	}

	public void setExponent(int exp)
	{
		this.exponent = exp;
	}
	public int getCoeficient() {
		return coeficient;
	}

	public void setCoeficient(int coef) {
		this.coeficient = coef;
	}
}


