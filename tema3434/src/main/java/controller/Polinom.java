package controller;

import java.util.*;

import model.Monom;

public class Polinom 
{
	ArrayList <Monom> pol=new ArrayList <Monom>();
	
	int l;
	
	public Polinom(String x)
	{
		int cf, exp,i;
		
		String p1=x.replaceAll("-", "+-");
		String[] p2=p1.split("\\+");
		
		for(i=0;i<p2.length;i++)
		{
			String[] y=p2[i].split("(x\\^)");
			cf= Integer.parseInt(y[0]);
			exp=Integer.parseInt(y[1]);
			if(cf!=0)
			{	 
				pol.add(new Monom(exp,cf));
				l++;
			}
		}
	}
	public void afisare()
	{
		for(Monom p2: pol)
		{
			if(p2.getCoeficient()!=0)
				System.out.println(p2.getCoeficient()+ " " + p2.getExponent()+ " ");
		}
			
	}
	public ArrayList<Monom> adunare(Polinom pol1)
	{
		ArrayList<Monom> pol2=new ArrayList<Monom>(10000);
		ArrayList<Monom> pol3=new ArrayList<Monom>(10000);
		
		int i,j=0,s=0,ok=0,okk=0,t=0;
		
		for(Monom p2: pol)
		{
			pol2.add(new Monom (p2.getExponent(),p2.getCoeficient()));
		}
		
		for(Monom p2: pol1.pol)
		{
			pol2.add(new Monom (p2.getExponent(),p2.getCoeficient()));
		}
		
		Collections.sort(pol2,new Comparator<Monom>()
		{
		
			public int compare(Monom x,Monom y)
			{
				Integer i=new Integer(y.getExponent());
				return i.compareTo(x.getExponent());
			}
		});
		for(i=0;i<pol2.size()-1;i++)
		{
			if(pol2.get(i).getExponent()==pol2.get(i+1).getExponent())
				ok=1;
		}
		if(ok==1)
		{
			for(i=0;i<pol2.size()-1;i++)
			{
				if(pol2.get(i).getExponent()==pol2.get(i+1).getExponent()&& okk!=1)
				{
					j=i+1;
					t=pol2.get(i).getExponent();
					s=pol2.get(i).getCoeficient();
					while (t==pol2.get(j).getExponent())
					{
						s=s+pol2.get(j).getCoeficient();
						j++;
						if(j==pol2.size())
							break;
					}
					pol3.add(new Monom(pol2.get(i).getExponent(),s));
					okk=1;
				}
				else
					if(pol2.get(i).getExponent()>pol2.get(i+1).getExponent() && okk!=1)
					{
						pol3.add(new Monom (pol2.get(i).getExponent(),pol2.get(i+1).getCoeficient()));
						
					}
					else
						if(i==(pol2.size()-2))
						{
							pol3.add(new Monom(pol2.get(i+1).getExponent(),pol2.get(i+1).getCoeficient()));
							
						}
						else
							if(pol2.get(i).getExponent()!=pol2.get(i+1).getExponent())
								okk=0;
			}
			return pol3;
		}
		return pol2;
	}
	public ArrayList<Monom> scadere(Polinom pol1)
	{
		ArrayList<Monom> pol2=new ArrayList<Monom>(10000);
		ArrayList<Monom> pol3=new ArrayList<Monom>(10000);
		
		int i,j=0,s=0,ok=0,okk=0,t=0;
		
		for(Monom p2: pol)
		{
			pol2.add(new Monom (p2.getExponent(),-p2.getCoeficient()));
		}
		
		for(Monom p2: pol1.pol)
		{
			pol2.add(new Monom (p2.getExponent(),-p2.getCoeficient()));
		}
		Collections.sort(pol2,new Comparator<Monom>()
		{
			
			public int compare(Monom x,Monom y)
			{
				Integer i=new Integer(y.getExponent());
				return i.compareTo(x.getExponent());
			}
		});
		for(i=0;i<pol2.size()-1;i++)
		{
			if(pol2.get(i).getExponent()==pol2.get(i+1).getExponent())
				ok=1;
		}
		if(ok==1)
		{
			for(i=0;i<pol2.size()-1;i++)
			{
				if(pol2.get(i).getExponent()==pol2.get(i+1).getExponent()&& okk!=1)
				{
					j=i+1;
					t=pol2.get(i).getExponent();
					s=pol2.get(i).getCoeficient();
					while (t==pol2.get(j).getExponent())
					{
						s=s-pol2.get(j).getCoeficient();
						j++;
						if(j==pol2.size())
							break;
					}
					pol3.add(new Monom(pol2.get(i).getExponent(),s));
					okk=1;
				}
				else
					if(pol2.get(i).getExponent()>pol2.get(i+1).getExponent() && okk!=1)
					{
						pol3.add(new Monom (pol2.get(i).getExponent(),pol2.get(i+1).getCoeficient()));
						
					}
					else
						if(i==(pol2.size()-2))
						{
							pol3.add(new Monom(pol2.get(i+1).getExponent(),pol2.get(i+1).getCoeficient()));
							
						}
						else
							if(pol2.get(i).getExponent()!=pol2.get(i+1).getExponent())
								okk=0;
			}
			return pol3;
		}
		return pol2;
	}
	
	public ArrayList<Monom> derivare()
	{
		ArrayList<Monom> pol2=new ArrayList<Monom>(10000);
		
		for(Monom p2: pol)
		{
			if(p2.getExponent()!=0)
				pol2.add(new Monom(p2.getExponent()-1,p2.getCoeficient()*p2.getExponent()));
		}
		
		Collections.sort(pol2,new Comparator<Monom>()
		{
			
			public int compare(Monom x,Monom y)
			{
				Integer i=new Integer(y.getExponent());
				return i.compareTo(x.getExponent());
			}
		});
		return pol2;
	}
	
	public ArrayList<Monom> integrare()
	{
		ArrayList<Monom> pol2=new ArrayList<Monom>(10000);
		for(Monom p2: pol)
		{
			int x1=p2.getExponent()+1;
			int x2=p2.getCoeficient()/(p2.getExponent()+1);
			pol2.add(new Monom(x1,x2));
			
		}

		Collections.sort(pol2,new Comparator<Monom>()
		{
			
			public int compare(Monom x,Monom y)
			{
				Integer i=new Integer(y.getExponent());
				return i.compareTo(x.getExponent());
			}
		});
		
		return pol2;
	}
	
	public ArrayList<Monom> inmultire(Polinom pol1)
	{
		ArrayList<Monom> pol2=new ArrayList<Monom>(100000);
		ArrayList<Monom> pol3=new ArrayList<Monom>(100000);
		int ok=0,okk=0,s=0,j=0,t=0,i;
		for(Monom p3: pol)
			for(Monom p4: pol1.pol)
			{
				pol2.add(new Monom(p3.getExponent()+p4.getExponent(),p3.getCoeficient()*p4.getCoeficient()));
			}
		Collections.sort(pol2,new Comparator<Monom>()
		{
			
			public int compare(Monom x,Monom y)
			{
				Integer i=new Integer(y.getExponent());
				return i.compareTo(x.getExponent());
			}
		});
		for(i=0;i<pol2.size()-1;i++)
		{
			if(pol2.get(i).getExponent()==pol2.get(i+1).getExponent())
				ok=1;
			if(ok==1)
			{
					for(i=0;i<pol2.size()-1;i++)
					{
						if(pol2.get(i).getExponent()==pol2.get(i+1).getExponent() && okk!=1)
						{
							j=i+1;
							t=pol2.get(i).getExponent();
							s=pol2.get(i).getCoeficient();
							while(t==pol2.get(j).getExponent())
							{
								s=s+pol2.get(j).getCoeficient();
								j++;
								if(j==pol2.size()) 
									break;
							}
							pol3.add(new Monom(pol2.get(i).getExponent(),s));
							okk=1;
								
						}
						else 
							if(pol2.get(i).getExponent()>pol2.get(i+1).getExponent() && okk!=1)
								pol3.add(new Monom(pol2.get(i).getExponent(),pol2.get(i).getCoeficient()));
					
							else
								if(i==(pol2.size()-2)) 
									pol3.add(new Monom(pol2.get(i+1).getExponent(),pol2.get(i+1).getCoeficient()));
					
								else 
									if(pol2.get(i).getExponent()!=pol2.get(i+1).getExponent()) 
										okk=0;
					}
					return pol3;
			}
		}
		return pol2;
	}
}


	
	
	
	


